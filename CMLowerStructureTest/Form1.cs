﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dino;
using CM18tool;
using System.Data.OleDb;
using System.IO;
using System.IO.Compression;

namespace CMLowerStructureTest
{
    public partial class Form1 : Form
    {
        Messages myMessage = new Messages();
        IniFile iniMessage = new IniFile(Application.StartupPath + "\\italiano.ini");
        IniFile iniConfig = new IniFile("setup.ini");
        public string myConnectionString = "";
        public OleDbConnection myConnection = new OleDbConnection();
        public OleDbCommand dbCommand;
        public OleDbDataReader dbReader;
        public DataTable dbDataTable = new DataTable();
        public string sqlCommand;
        public const string PASS = "PASS";
        public const string FAIL = "FAIL";
        //CM18tool.CmDLLs myDll = new CM18tool.CmDLLs();
        //CM18tool.CmDLLs.ConnectionParam cmConnectionParam = new CM18tool.CmDLLs.ConnectionParam();
        CM18tool.CmDLLs.ConnectionParam cmConnectionParam;
        CM18tool.CmDLLs myDll;
        Arca myFunc = new Arca();
        int lastTest = 0;
        List<Label> lblTest;
        string elmTest = "";
        string testResult = "FAIL";
        int fCashLMin;
        int fCashLMax;
        int fCashRMin;
        int fCashRMax;
        string fwFolder = "";

        struct SLowerStructure
        {
            public string productCode;
            public string clientName;
            public int clientId;
            public string moduleCode;
            public string moduleSerialNumber;
            public string suiteCode;
            public string fwFileName;
            public string safeVerInstalled;
            public string safeRelInstalled;
            public string chekViteria;
            public string chekConnessioni;
            public string checkIngresso;
            public string connAllarmi;
            public string connInterlock;
            public string asmCassettes;
            public string powerOn;
            public string fwInstalled;
            public string chekStato;
            public string testMivIn;
            public string testMivOut;
            public string adjFotoCash;
            public int fCashLValue;
            public int fCashRValue;
            public string testVassoio;
            public string testElm;
            public string testRESULT;
            public string productFamily;
            public string cassetteNumber;
            //public List<SLowerTest> test;
        }
        struct SLowerTest
        {
            public string name;
            public string result;
            public string value;
        }

        SLowerStructure myLowerStructure = new SLowerStructure();
        public Form1()
        {
            InitializeComponent();
            InitializeMyComponent();
        }

        //string messaggio(int idMessage)
        //{
        //    return iniMessage.GetValue("messaggi", idMessage.ToString());
        //}
        void messaggio (int idMessage)
        {
            lblMessage.Text= iniMessage.GetValue("messaggi", idMessage.ToString());
        }
        void messaggio(int idMessage, params object[] param)
        {
            lblMessage.Text = string.Format(iniMessage.GetValue("messaggi", idMessage.ToString()),param);
        }
        public void CmDataSent(object sender, EventArgs e)
        {
            lstComScope.Items.Add("SND:" + sender);
            lstComScope.SetSelected(lstComScope.Items.Count - 1, true);
            this.Refresh();
        }

        public void CmDataReceived(object sender, EventArgs e)
        {
            lstComScope.Items.Add("RCV:" + sender);
            lstComScope.SetSelected(lstComScope.Items.Count - 1, true);
            this.Refresh();
        }

        void InitializeMyComponent()
        {
            myDll = new CM18tool.CmDLLs();
            cmConnectionParam = new CM18tool.CmDLLs.ConnectionParam();
            myDll.CommandSent += new EventHandler(CmDataSent);
            myDll.CommandReceived += new EventHandler(CmDataReceived);
            cmConnectionParam.ConnectionMode = CM18tool.CmDLLs.DLINK_MODE_SERIAL;
            cmConnectionParam.pRsConf.baudrate = 9600;
            cmConnectionParam.pRsConf.device = "COM1";
            cmConnectionParam.pRsConf.parity = 0;
            cmConnectionParam.pRsConf.car = 8;
            cmConnectionParam.pRsConf.stop = 1;

            string serverDB = iniConfig.GetValue("link", "database");
            string localDB = Application.StartupPath + "\\DB\\man1220_dati.mdb";
            if (!File.Exists(serverDB) && !File.Exists(localDB))
                goto dbConnectionString;
            if (File.Exists(serverDB) )
            {
                if (File.GetLastWriteTime(serverDB) > File.GetLastWriteTime(localDB))
                {
                    if (!Directory.Exists(Application.StartupPath + "\\DB"))
                        Directory.CreateDirectory(Application.StartupPath + "\\DB");
                    File.Copy(serverDB, localDB, true);
                }
            }
            dbConnectionString:
            //this.Text = messaggio(1); //Collaudo struttura inferiore
            myConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;" +
            "Data Source=" + localDB +
            "; Persist Security Info=True;" + "Jet OLEDB:Database Password=;";
            sqlCommand = "SELECT DISTINCT prodotti.id_prodotto FROM prodotti,tblman WHERE (prodotti.categoria_prodotto LIKE 'CM18%' OR prodotti.categoria_prodotto = 'OM61') AND (prodotti.id_prodotto = tblman.id_prodotto) ORDER BY prodotti.id_prodotto";
            using (myConnection = new OleDbConnection(myConnectionString))
            {
                dbCommand = new OleDbCommand(sqlCommand,myConnection);
                try
                {
                    myConnection.Open();
                    dbReader = dbCommand.ExecuteReader();
                    while(dbReader.Read())
                    {
                        cboPartNumber.Items.Add(dbReader[0].ToString());
                        //Console.WriteLine(dbReader[0].ToString());
                    }
                    dbReader.Close();
                }
                catch (Exception ex)
                {
                    switch (ex.HResult)
                    {
                        case -2147467259:
                            MessageBox.Show("Non è stato possibile connettersi al DBFW (sia locale che remoto), Non è possibile selezionare nessun pn da collaudare.","ATTENZIONE",MessageBoxButtons.OK,MessageBoxIcon.Error);
                            break;
                        default:
                            MessageBox.Show(ex.ToString());
                            break;
                    }
                    
                }
            }
        }

        void FillDbInControl(string SQL, Control ctrl)
        {
            if (ctrl is ComboBox)
            {
                ComboBox myList = new ComboBox();
                myList = (ComboBox)ctrl;
                using (myConnection = new OleDbConnection(myConnectionString))
                {
                    dbCommand = new OleDbCommand(SQL, myConnection);
                    try
                    {
                        myConnection.Open();
                        dbReader=dbCommand.ExecuteReader();
                        while (dbReader.Read())
                        {
                            myList.Items.Add(dbReader[0].ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
        }
        OleDbDataReader GetDataDB(string SQL)
        {
            using (myConnection = new OleDbConnection(myConnectionString))
            {
                dbCommand = new OleDbCommand(SQL, myConnection);
                try
                {
                    myConnection.Open();
                    return dbCommand.ExecuteReader();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            return null;

        }
        private void Form1_Load(object sender, EventArgs e)
        {
            string portName;
            messaggio(2); //Seleziona il part number
            lblTest = new List<Label>();
            fwFolder = iniConfig.GetValue("link", "fwFolder");
            portName = iniConfig.GetValue("connection", "portname");
            if (portName.ToUpper() == "USB")
                cmConnectionParam.ConnectionMode = CM18tool.CmDLLs.DLINK_MODE_USB;
            else
                cmConnectionParam.pRsConf.device = portName;
            int i = 0;
            while (iniConfig.GetValue("testlist",i.ToString())!="")
            {
                Label lbl = new Label();
                lbl.Name = "lblTest" + i.ToString();
                lbl.Text = iniConfig.GetValue("testlist", i.ToString()).ToUpper();
                lbl.Left = 15;
                lbl.Top = 35 + (i * 19);
                //lbl.BackColor = Color.Cyan;
                lbl.AutoSize = true;
                lbl.Tag = i.ToString();
                lbl.BackColor = Color.Blue;
                lbl.ForeColor = Color.LightSeaGreen;
                lblTest.Add(lbl);
                splitContainer1.Panel1.Controls.Add(lblTest[i]);
                lastTest = i;
                i += 1;
            }
            cboPartNumber.Focus();
        }

        string LowerStructureTest()
        {
            string testResult = PASS;

            return testResult;
        }
        enum MessageType
        {
            PassFail,
            Simple
        }
        //string SingleTest(int idLabel, int idMessage, MessageType messageType = MessageType.PassFail)
        //{
        //    string result = "";
        //    string message = iniMessage.GetValue("messaggi", idMessage.ToString());
        //    lblTest[idLabel].BackColor = Color.Red;
        //    switch(messageType)
        //    {
        //        case MessageType.PassFail:
        //            result = myMessage.PassFail(message);
        //            break;
        //    }
        //    if (result=="PASS")
        //        lblTest[idLabel].BackColor = Color.Lime;
        //    return result;
        //}
        private int AttendiTasto()
        {
            //this.Text = Arca.keyCodePressed.ToString();
            Arca.keyCodePressed = 0;
            while (Arca.keyCodePressed == 0)
            {
                
                System.Threading.Thread.Sleep(100);
                Application.DoEvents();
            }
            return Arca.keyCodePressed;
        }

        string SingleTest (int idMessage, int idTest, Image image = null)
        {
            start:
            picImage.Image = null;
            if (image != null)
            {
                picImage.Visible = true;
                picImage.Image = image;
            }
            lblTest[idTest].BackColor = Color.Red;
            string result = PASS;
            messaggio(idMessage);
            if (AttendiTasto() == 27)
            {
                result = FAIL;
            }
            //else
            //    goto start;
            if (result==PASS)
                lblTest[idTest].BackColor = Color.Lime;
            picImage.Image = null;
            picImage.Visible = false;
            return result;
        }

        string FirstPowerOn(int testId = 0)
        {
            string result = FAIL;
            string cmReply;
            messaggio(26);//Accendi la macchina ed attendi la fine del recovery. Premi un tasto per continuare, ESC per uscire
            lblTest[testId].BackColor = Color.Red;
            if (AttendiTasto() == 27)
                goto fine;
            primaAccensione:
            if (myDll.CMConnect(ref cmConnectionParam) != 0)
            {
                messaggio(10);//Connessione non riuscita. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
                goto primaAccensione;
            }
            //cmReply = myDll.CMSingleCommand("T,1,0,7", 4);
            int contatore = 0;
            primaConnessione:
            messaggio(11); //Connessione in corso
            cmReply = myDll.CMSingleCommand("O,1,L,123456", 3);
            if (cmReply == "1059" || cmReply == "102" || cmReply == "4")
            {
                // aspetta un secondo, aggiungi contatore, riprova. massimo 40 secondi
                if (contatore > 70)
                {
                    messaggio(10);//Connessione non riuscita. Premere un tasto per ripetere, ESC per uscire
                    if (AttendiTasto() == 27)
                        goto fine;
                    goto primaAccensione;
                }
                contatore += 1;
                if (cmReply == "1059")
                    contatore += 4;
                Arca.WaitingTime(1000);
                goto primaConnessione;
                //nessuna risposta
            }
            result = PASS;
            myLowerStructure.powerOn = PASS;
            lblTest[testId].BackColor = Color.Lime;
            fine:
            myDll.CMSingleCommand("C,1,L");
            myDll.CMDisconnect();
            return result;
        }

        string MachineReadyTest(int testId = 1)
        {
            string result = FAIL;
            string cmReply;
            lblTest[testId].BackColor = Color.Red;

            connessione:
            if (myDll.CMConnect(ref cmConnectionParam) != 0)
            {
                messaggio(10);//Connessione non riuscita. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
                goto connessione;
            }
            messaggio(27);//Verifica che la macchina sia in 'READY' in corso
            cmReply = myDll.CMSingleCommand("O,1,L,123456", 3);

            if (cmReply != "1" && cmReply != "101")
            {
                messaggio(12, cmReply); //La macchina è in stato {0}. Correggi il problema e premi un tasto per riprovare, ESC per uscire
                myDll.CMDisconnect();
                if (AttendiTasto() == 27)
                    goto fine;
                goto connessione;
            }
            result = PASS;
            lblTest[testId].BackColor = Color.Lime;
            myLowerStructure.chekStato = PASS;
            fine:
            cmReply = myDll.CMSingleCommand("C,1,L", 3);
            myDll.CMDisconnect();
            return result;
        }

        string InstallFW(int testId = 2)
        {
            string result = FAIL;
            string zipFileName = "";
            string folderSuiteName = "";
            string downloadFolder = Application.StartupPath + "\\Download";
            string fwSafeName = "";
            string fwFileVer = "0";
            string fwFileRel="0";
            lblTest[testId].BackColor = Color.Red;

            /*
             * ricerca cartella suite
             * ricerca file zip
             * estrazione file zip
             * ricerca file SBxx o SCxx (CM18B/OM61 o altri)
             * messaggio file da installare
             */

            // crea cartella locale e la svuota
            if (Directory.Exists(downloadFolder) == false)
                Directory.CreateDirectory(downloadFolder);
            DirectoryInfo dirInfo = new DirectoryInfo(downloadFolder);
            foreach (FileInfo file in dirInfo.GetFiles())
                file.Delete();

            // ricerca cartella suite
            folderSuiteName = fwFolder + myLowerStructure.suiteCode;
            if (Directory.Exists(folderSuiteName) == false)
            {
                messaggio(12,myLowerStructure.suiteCode,fwFolder); //Non e' stato possibile trovare la cartella del FW ({0}) in archivio ({1}). Premi un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
            }
            // ricerca del file zip
            dirInfo = new DirectoryInfo(@folderSuiteName);
            foreach (FileInfo file in dirInfo.GetFiles())
            {
                if (file.Extension.ToLower() == ".zip")
                {
                    zipFileName = file.FullName;
                    break;
                }
            }
            if (zipFileName == "")
            {
                messaggio(13, folderSuiteName); //Non e' stato possibile trovare il file zip nella cartella {0}. Premi un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
            }
            // estrazione del file zip
            ZipFile.ExtractToDirectory(zipFileName, downloadFolder);

            // ricerca del file SB o SC nella cartella download
            dirInfo = new DirectoryInfo(downloadFolder);
            foreach (FileInfo file in dirInfo.GetFiles())
            {
                if (file.Name.StartsWith("SB") || file.Name.StartsWith("SC"))
                {
                    fwSafeName = file.Name;
                    fwFileVer = fwSafeName.Substring(5, 4);
                    fwFileRel = fwSafeName.Substring(10, 4);
                    break;
                }
            }
            if (fwSafeName == "")
            {
                messaggio(14, folderSuiteName); //Non e' stato possibile trovare nessun file idoneo (SBxx o SCxx) nella cartella locale. Premi un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
            }
            // visualizza il messaggio del file da installare dalla SD

            messaggio(28, fwSafeName, downloadFolder);//Installa il fw {0} da display. Se non è presente nella SD è possibile prenderlo dalla cartella {1}. Premere un tasto a download terminato, ESC per uscire
            if (AttendiTasto() == 27)
                goto fine;
            

            
            string cmReply = "";
            connessione:
            int cmConReply = myDll.CMConnect(ref cmConnectionParam);
            if (cmConReply != 0)
            {
                if (cmConReply == 1052)
                {
                    myDll.CMDisconnect();
                    goto connessione;
                }
                messaggio(30, cmConReply); //Non è stato possibile connettersi alla seriale, risposta {0}. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
            }
            verificaFw:
            cmReply = myDll.CMSingleCommand("V,1,4", 2);
            if (cmReply != "101" && cmReply != "1")
            {
                messaggio(56, cmReply); //Non è stato possibile verificare la versione del SAFE, risposta {0}. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
                goto verificaFw;
            }
            myLowerStructure.safeVerInstalled = myDll.commandSingleAns[3];
            myLowerStructure.safeRelInstalled = myDll.commandSingleAns[4];

            if (myLowerStructure.safeRelInstalled != fwFileRel || myLowerStructure.safeVerInstalled != fwFileVer)
            {
                messaggio(60, myLowerStructure.safeVerInstalled, myLowerStructure.safeRelInstalled, fwFileVer, fwFileRel); //Firmware installato ({0}_{1}) diverso da quello atteso ({2}_{3}). Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
                goto verificaFw;
            }

            result = PASS;
            lblTest[testId].BackColor = Color.Lime;
            myLowerStructure.fwInstalled = PASS;
            myLowerStructure.fwFileName = fwSafeName;
            fine:
            myDll.CMDisconnect();
            dirInfo = new DirectoryInfo(downloadFolder);
            foreach (FileInfo file in dirInfo.GetFiles())
                file.Delete();
            return result;
        }

        string SetCassetteNumber(int testId = 3)
        {
            lblTest[testId].BackColor = Color.Red;
            string result = FAIL;
            string cmReply;
            int cassetteNumber = 8;
            myLowerStructure.productFamily = myLowerStructure.productFamily.ToUpper();
            if (myLowerStructure.productFamily == "CM18" || myLowerStructure.productFamily == "CM18T" || myLowerStructure.productFamily == "OM61" 
                || myLowerStructure.productFamily == "CM18B" || myLowerStructure.productFamily == "CM18EVOT" || myLowerStructure.productFamily == "CM18EVOT"
                || myLowerStructure.productFamily == "CM18SOLO" || myLowerStructure.productFamily == "CM18SOLOT")
                goto FillCassetteNumber;
            int spaceIndex = myLowerStructure.productFamily.IndexOf(" ");
            if (spaceIndex > 4)
                myLowerStructure.productFamily = myLowerStructure.productFamily.Substring(0, myLowerStructure.productFamily.IndexOf(" "));


            FillCassetteNumber:
            switch (myLowerStructure.productFamily)
            {
                case "CM18":
                case "CM18SOLO":
                case "CM18EVO":
                    cassetteNumber = 8;
                    break;
                case "CM18B":
                case "OM61":
                    cassetteNumber = 6;
                    break;
                case "CM18T":
                case "CM18EVOT":
                case "CM18SOLOT":
                    cassetteNumber = 12;
                    break;
                default:
                    cassetteNumber = 8;
                    break;
            }

            connessione:
            int cmConReply = myDll.CMConnect(ref cmConnectionParam);
            if (cmConReply != 0)
            {
                if (cmConReply == 1052)
                {
                    myDll.CMDisconnect();
                    goto connessione;
                }
                messaggio(30, cmConReply); //Non è stato possibile connettersi alla seriale, risposta {0}. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
                goto connessione;
            }
            //F,n,11,cassette nbumber
            fillCommand:
            cmReply= myDll.CMSingleCommand("F,1,11," + cassetteNumber.ToString(), 3);
            if (cmReply != "101" && cmReply != "1")
            {
                messaggio(57,cassetteNumber.ToString(), cmReply); //Non è stato possibile impostare il numero dei cassetti ({0}). Stato macchina: {1}. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
                goto fillCommand;
            }

            getCommand:
            string getCassetteNumber = "";
            cmReply = myDll.CMSingleCommand("T,1,0,61", 4);
            if (cmReply != "101" && cmReply != "1")
            {
                messaggio(58, cassetteNumber.ToString(), cmReply); //Non è stato possibile leggere il numero dei cassetti impostati. Stato macchina: {1}. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
                goto getCommand;
            }
            getCassetteNumber = myDll.commandSingleAns[5];
            myLowerStructure.cassetteNumber = getCassetteNumber;
            if (getCassetteNumber != cassetteNumber.ToString())
            {
                messaggio(59, getCassetteNumber, cassetteNumber.ToString()); //Impostazione a {1} cassetti non riuscita. Rilevato settaggio a {0}. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
                goto fillCommand;
            }
            result = PASS;
            lblTest[testId].BackColor = Color.Lime;
            fine:
            messaggio(47); //Disconnessione dalla seriale in corso
            myDll.CMDisconnect();
            return result;
        }

        string TestMotorMiv(int testInId = 4, int testOutId = 5)
        {
            string result = FAIL;
            string cmReply;

            // prova motore MIV IN
            messaggio(15);//Attivazione motore MIV - IN
            lblTest[testInId].BackColor = Color.Red;
            connessione:
            int cmConReply = myDll.CMConnect(ref cmConnectionParam);
            if (cmConReply != 0)
            {
                if (cmConReply == 1052)
                {
                    myDll.CMDisconnect();
                    goto connessione;
                }
                messaggio(30, cmConReply); //Non è stato possibile connettersi alla seriale, risposta {0}. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
            }
            trasparentIn:
            messaggio(44); //TRANSPARENT IN in corso
            cmReply = myDll.CMTransparentIn();
            Arca.WaitingTime(3000);
            if (cmReply != "101" && cmReply != "1")
            {
                messaggio(29, cmReply); //Non è stato possibile entrare in TRANSPARENT, risposta {0}. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
                goto trasparentIn;
            }
            potOn:
            messaggio(46); //Attivazione potenza in corso
            cmReply = myDll.CMTransparentCommand("D133");
            if (cmReply != "40" )
            {
                messaggio(37, cmReply); //Attivazione potenza non andata a buon fine. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
                goto potOn;
            }
            mivOnIn:
            messaggio(31); // Attivazione motore MIV IN in corso
            cmReply = myDll.CMTransparentCommand("D107");
            if (cmReply != "40")
            {
                messaggio(38, cmReply); //Attivazione motore MIV IN non andata a buon fine. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
                goto mivOnIn;
            }
            messaggio(32); //Verifica che il motore MIV stia girando nel verso IN. Premi un tasto per continuare, ESC per uscire
            if (AttendiTasto() == 27)
                goto fine;

            mivOffIn:
            messaggio(35); //Disattivazione motore MIV in corso
            cmReply = myDll.CMTransparentCommand("D109");
            if (cmReply != "40")
            {
                messaggio(36, cmReply); //Disattivazione motore MIV IN non andata a buon fine. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
                goto mivOffIn;
            }
            lblTest[testInId].BackColor = Color.Lime;
            myLowerStructure.testMivIn= PASS;

            // prova motore MIV OUT
            lblTest[testOutId].BackColor = Color.Red;
            mivOnOut:
            messaggio(33); //Attivazione motore MIV OUT in corso
            cmReply = myDll.CMTransparentCommand("D108");
            if (cmReply != "40")
            {
                messaggio(39, cmReply); //Attivazione motore MIV OUT non andata a buon fine. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
                goto mivOnOut;
            }
            messaggio(34); //Verifica che il motore MIV stia girando nel verso OUT. Premi un tasto per continuare, ESC per uscire
            if (AttendiTasto() == 27)
                goto fine;
            mivOffOut:
            messaggio(35); //Disattivazione motore MIV in corso
            cmReply = myDll.CMTransparentCommand("D109");
            if (cmReply != "40")
            {
                messaggio(40, cmReply); //Disattivazione motore MIV OUT non andata a buon fine. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
                goto mivOffOut;
            }
            lblTest[testOutId].BackColor = Color.Lime;
            myLowerStructure.testMivOut= PASS;

            potOff:
            messaggio(43); //Disattivazione potenza in corso
            cmReply = myDll.CMTransparentCommand("D134");
            if (cmReply != "40")
            {
                messaggio(41, cmReply); //Disttivazione potenza non andata a buon fine. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
                goto potOff;
            }

            transparentOut:
            messaggio(45); //TRANSPARENT OUT in corso
            cmReply = myDll.CMTransparentOut();
            if (cmReply != "101" && cmReply != "1")
            {
                messaggio(42, cmReply); //Non è stato possibile uscire dal TRANSPARENT, risposta {0}. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
                goto transparentOut;
            }

            result = PASS;
            fine:
            messaggio(45); //TRANSPARENT OUT in corso
            myDll.CMTransparentOut();
            messaggio(47); //Disconnessione dalla seriale in corso
            myDll.CMDisconnect();
            return result;
        }

        string TaraturaFoto(int testId = 6)
        {
            string result = FAIL;
            string cmReply = "";

            messaggio(17);//Taratura dei foto cash L+R in corso
            lblTest[testId].BackColor = Color.Red;
            connessione:

            int cmConReply = myDll.CMConnect(ref cmConnectionParam);
            if (cmConReply != 0)
            {
                if (cmConReply == 1052)
                {
                    myDll.CMDisconnect();
                    goto connessione;
                }
                messaggio(30, cmConReply); //Non è stato possibile connettersi alla seriale, risposta {0}. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
            }

            taraturaCashL:
            messaggio(48); //Taratura foto CASH L in corso
            cmReply = myDll.CMSingleCommand("T,1,0,60,0,30", 5);
            if (cmReply != "101" && cmReply != "1")
            {
                messaggio(29, cmReply); //Non è stato possibile entrare in TRANSPARENT, risposta {0}. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
                goto taraturaCashL;
            }

            taraturaCashR:
            messaggio(49); //Taratura foto CASH R in corso
            cmReply = myDll.CMSingleCommand("T,1,0,60,0,31", 5);
            if (cmReply != "101" && cmReply != "1")
            {
                messaggio(29, cmReply); //Non è stato possibile entrare in TRANSPARENT, risposta {0}. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
                goto taraturaCashR;
            }

            letturaTaratura:
            messaggio(50); // Lettura delle tarature dei foto in corso
            cmReply = myDll.CMSingleCommand("T,1,0,60,1", 5);
            if (cmReply != "101" && cmReply != "1")
            {
                messaggio(29, cmReply); //Non è stato possibile entrare in TRANSPARENT, risposta {0}. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
                goto letturaTaratura;
            }

            int fcashL, fcashR;
            fCashLMin = iniConfig.GetInteger("photorange", "cashlmin");
            fCashLMax = iniConfig.GetInteger("photorange", "cashlmax");
            fCashRMin = iniConfig.GetInteger("photorange", "cashrmin");
            fCashRMax = iniConfig.GetInteger("photorange", "cashrmax");
            fcashL = Convert.ToInt16(myDll.commandSingleAns[26]);
            fcashR = Convert.ToInt16(myDll.commandSingleAns[27]);
            myLowerStructure.fCashLValue = fcashL;
            myLowerStructure.fCashRValue = fcashR;
            if (fcashL > fCashLMax || fcashL < fCashLMin || fcashR > fCashRMax || fcashR < fCashRMin)
            {
                messaggio(7); //Taratura foto FALLITA. Premi un tasto per ripetere, ESC per uscire
                dgTable.Rows.Clear();
                dgTable.ColumnHeadersVisible = false;
                dgTable.RowHeadersVisible = false;
                dgTable.ColumnCount = 4;
                dgTable.Rows.Add("Photo Name", "Value", "Min", "Max");
                dgTable.Rows.Add("CASH L", fcashL, fCashLMin, fCashLMax);
                dgTable.Rows.Add("CASH R", fcashR, fCashRMin, fCashRMax);
                dgTable.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
                dgTable.Visible = true;
                if (AttendiTasto() == 27)
                    goto fine;
                dgTable.Visible = false;
                goto taraturaCashL;
            }
            myDll.CMDisconnect();
            myLowerStructure.adjFotoCash = PASS;
            lblTest[testId].BackColor = Color.Lime;
            result = PASS;
            fine:
            dgTable.Visible = false;
            myDll.CMDisconnect();
            return result;
        }

        string TestAperturaVassoio(int testId = 8)
        {
            string result = FAIL;
            picImage.Image = null;
            lblTest[testId].BackColor = Color.Red;
            messaggio(51); //Premi lo switch del CAB- per simulare l'apertura della porta, il buzzer deve emettere un suono intermittente. Premi un tasto per continuare, ESC per uscire
            picImage.Visible = true;
            picImage.Image = Properties.Resources.MicroSwitch;
            if (AttendiTasto() == 27)
                goto fine;
            messaggio(52); //Scollega un cassetto 'finto', il buzzer deve aumentare la velocità dell'intermittenza. Premi un tasto per continuare, ESC per uscire
            picImage.Visible = false;
            if (AttendiTasto() == 27)
                goto fine;
            result = PASS;
            lblTest[testId].BackColor = Color.Lime;
            myLowerStructure.testVassoio = PASS;

            fine:
            picImage.Image = null;
            picImage.Visible = false;

            return result;
        }

        void InizializzaTestResult()
        {
            myLowerStructure.chekViteria = FAIL;
            myLowerStructure.chekConnessioni = FAIL;
            myLowerStructure.checkIngresso = FAIL;
            myLowerStructure.asmCassettes = FAIL;
            myLowerStructure.connAllarmi = FAIL;
            myLowerStructure.connInterlock = FAIL;
            myLowerStructure.powerOn = FAIL;
            myLowerStructure.chekStato = FAIL;
            myLowerStructure.fwInstalled = FAIL;
            myLowerStructure.testMivIn = FAIL;
            myLowerStructure.testElm = "NOT TESTED";
            myLowerStructure.testVassoio = FAIL;
            myLowerStructure.adjFotoCash = FAIL;
            myLowerStructure.testMivOut = FAIL;
            myLowerStructure.fCashLValue = 0;
            myLowerStructure.fCashRValue = 0;
            myLowerStructure.testRESULT = FAIL;
            myLowerStructure.fwFileName = "NOT INSTALLED";
            myLowerStructure.safeRelInstalled = "-";
            myLowerStructure.safeVerInstalled = "-";
        }
        private void btnStart_Click(object sender, EventArgs e)
        {
            btnStart.Visible = false;
            Arca.keyCodePressed = 0;
            InizializzaTestResult();
            //if (SingleTest(20, 0) == FAIL)//Verificare che le viti siano fissate correttamente, premi un tasto per continuare, ESC per uscire
            //    goto fine;
            //myLowerStructure.chekViteria = PASS;

            //if (SingleTest(21, 1) == FAIL)//Verificare che i connettori ed i cavi flat siano connessi correttamente. Premi un tasto per continuare, ESC per uscire
            //    goto fine;
            //myLowerStructure.chekConnessioni = PASS;

            //if (SingleTest(22, 2) == FAIL)//Collega il gruppo ingresso bn. Premi un tasto per continuare, ESC per uscire
            //    goto fine;
            //myLowerStructure.checkIngresso = PASS;

            //if (SingleTest(23, 3) == FAIL)//Collega i cassetti finti. Premi un tasto per continuare, ESC per uscire
            //    goto fine;
            //myLowerStructure.asmCassettes = PASS;

            //if (SingleTest(24, 4) == FAIL)//Collega la piastra allarmi. Premi un tasto per continuare, ESC per uscire
            //    goto fine;
            //myLowerStructure.connAllarmi = PASS;

            //if (SingleTest(25, 5) == FAIL)//Collega l'interlock 'volante'. Premi un tasto per continuare, ESC per uscire
            //    goto fine;
            //myLowerStructure.connInterlock = PASS;

            if (FirstPowerOn() == FAIL)
                goto fine;

            if (MachineReadyTest() == FAIL)
                goto fine;

            if (InstallFW() == FAIL)
                goto fine;

            if (SetCassetteNumber() == FAIL)
                goto fine;

            if (TestMotorMiv() == FAIL)
                goto fine;

            if (TaraturaFoto() == FAIL)
                goto fine;

            if (elmTest == "1")
            {
                if (TestElm() == FAIL)
                    goto fine;
            }

            if (TestAperturaVassoio() == FAIL)
                goto fine;

            

            /*Mail dado 04/02/21
             * test da fare guidati:1: adjust foto cash? (ingressi)
                               2: MIV-IN /MIV-OUT
                               3: test magnete apertura porta (credo solo su 43658 ,ma non  sono sicuro al 100 %)
                               4: simulazione vassoio aperto. 
                               Ovviamente all'inizio del test download dalla rete a secondo del par-number immesso...
*/
            testResult = "PASS";
            fine:
            myLowerStructure.testRESULT = testResult;
            SalvaLog();
            SalvaLogAccess();
            picImage.Image = null;
            picImage.Visible = false;
            dgTable.Visible = false;
            messaggio(6);// Test FALLITO!Premi un tasto per continuare
            if (testResult=="PASS")
                messaggio(5); //Test eseguito correttamente. Premi un tasto per continuare

            testResult = "FAIL";
            AttendiTasto();
            RipristinaForm();
        }

        void RipristinaForm()
        {
            foreach (Control ctrl in splitContainer1.Panel1.Controls)
            {
                if (ctrl is Label)
                    if (  ctrl.Text != "Test List" && ctrl.Text != "Com Scope")
                        ctrl.BackColor = Color.Blue;
            }
            messaggio(2); //Seleziona il part number
            cboClient.Items.Clear();
            cboClient.Text = "";
            cboClient.Enabled = false;
            cboClient.Visible = true;
            cboPartNumber.Enabled = true;
            cboPartNumber.Text = "";
            cboPartNumber.Visible = true;
            txtCode.Text = "";
            cboPartNumber.Focus();
        }

        void SalvaLogAccess()
        {
            OleDbConnection conn = new OleDbConnection();
            conn.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + iniConfig.GetValue("link", "logdb");
            string sql = "INSERT INTO tblLowerStructure (ProductCode,ProductFamily,CassetteNumber,ModuleCode,SerialNumber,TestDate,TestTime,SwVersion,TestResult,Connections,InputBN,CassettesInstall,AlarmBoard,Interlock,PowerOn,MachineState,FwInstall,SuiteCode,FwFileName,FwSafeVer,FwSafeRel,MivIn,MivOut,PhotoCashAdj,CashLvalue,CashRvalue,TrayOpen,Elm,PcName)" +
                " VALUES ('" + myLowerStructure.productCode + "','" + myLowerStructure.productFamily + "','" + myLowerStructure.cassetteNumber  + "','" + myLowerStructure.moduleCode + "','" + myLowerStructure.moduleSerialNumber + "','" +
                DateTime.Now.ToShortDateString() + "','" + DateTime.Now.ToShortTimeString() + "','" + Application.ProductVersion + "','" +
                myLowerStructure.testRESULT + "','" + myLowerStructure.chekConnessioni + "','" + myLowerStructure.checkIngresso + "','" +
                myLowerStructure.asmCassettes + "','" + myLowerStructure.connAllarmi + "','" + myLowerStructure.connInterlock + "','" +
                myLowerStructure.powerOn + "','" + myLowerStructure.chekStato + "','" + myLowerStructure.fwInstalled + "','" + myLowerStructure.suiteCode + "','" +
                myLowerStructure.fwFileName + "','" + myLowerStructure.safeVerInstalled + "','" + myLowerStructure.safeRelInstalled + "','" + myLowerStructure.testMivIn + "','" +
                myLowerStructure.testMivOut + "','" + myLowerStructure.adjFotoCash + "'," + myLowerStructure.fCashLValue + "," + myLowerStructure.fCashRValue + ",'" +
                myLowerStructure.testVassoio + "','" + myLowerStructure.testElm + "','" + Environment.MachineName + "')";
            OleDbCommand oleCommand = new OleDbCommand();
            try
            {
                oleCommand.CommandText = sql;
                oleCommand.Connection = conn;
                oleCommand.Connection.Open();
                oleCommand.ExecuteNonQuery();
            }
            catch { }
            conn.Close();

        }

        void SalvaLog()
        {
            string localLogForlder = Application.StartupPath + "\\LOG";
            if (!Directory.Exists(localLogForlder))
                Directory.CreateDirectory(localLogForlder);

            string logLine = "";
            string logFile = localLogForlder + "\\logCollaudi.txt";
            if (!File.Exists(logFile))
            {
                using (StreamWriter myLogFile = new StreamWriter(localLogForlder + "\\logCollaudi.txt"))
                {
                    logLine = "ProductCode,ModuleCode,ModuleSN,Date,Time,SwVersion,Connections,InputBN,CassettesInstall,AlarmBoard,Interlock,PowerOn," +
                        "MachineState,FwInstall,SuiteCode,FwFileName,SafeFwVer,SafeFwRel,MivIn,MivOut,PCashAdj,chasLValue,cashRValue,TrayOpen,Elm";
                    myLogFile.WriteLine(logLine);
                    logLine = "";
                }
            }

            logLine += myLowerStructure.productCode +"," + myLowerStructure.moduleCode + "," + myLowerStructure.moduleSerialNumber + "," + 
                DateTime.Now.ToShortDateString() + "," + DateTime.Now.ToShortTimeString() + "," + Application.ProductVersion + "," + 
                myLowerStructure.testRESULT + "," + myLowerStructure.chekConnessioni + "," + myLowerStructure.checkIngresso + "," + 
                myLowerStructure.asmCassettes + "," + myLowerStructure.connAllarmi + "," + myLowerStructure.connInterlock + "," + 
                myLowerStructure.powerOn + "," + myLowerStructure.chekStato + "," + myLowerStructure.fwInstalled + "," + myLowerStructure.suiteCode + "," + 
                myLowerStructure.fwFileName + "," + myLowerStructure.safeVerInstalled + "," + myLowerStructure.safeRelInstalled + "," + myLowerStructure.testMivIn + "," + 
                myLowerStructure.testMivOut + "," + myLowerStructure.adjFotoCash + "," + myLowerStructure.fCashLValue + "," + myLowerStructure.fCashRValue + "," + 
                myLowerStructure.testVassoio + "," + myLowerStructure.testElm;

            using (StreamWriter myLogFile = File.AppendText(logFile))
            {
                myLogFile.WriteLine(logLine);
            }
            string lineSingleLog = "";
            lineSingleLog += "*********************************************" + Environment.NewLine;
            lineSingleLog += "*        CMxx LOWER STRUCTURE TEST          *" + Environment.NewLine;
            lineSingleLog += "*********************************************" + Environment.NewLine;
            lineSingleLog += "" + Environment.NewLine;
            lineSingleLog += "Product Code:          " + myLowerStructure.productCode + Environment.NewLine;
            lineSingleLog += "Product Family:        " + myLowerStructure.productFamily + Environment.NewLine;
            lineSingleLog += "Cassette Number:       " + myLowerStructure.cassetteNumber + Environment.NewLine;
            lineSingleLog += "Module Code:           " + myLowerStructure.moduleCode + Environment.NewLine;
            lineSingleLog += "Serial Number:         " + myLowerStructure.moduleSerialNumber + Environment.NewLine;
            lineSingleLog += "Date:                  " + DateTime.Now.ToShortDateString() + Environment.NewLine;
            lineSingleLog += "Time:                  " + DateTime.Now.ToShortTimeString() + Environment.NewLine;
            lineSingleLog += "SW version:            " + Application.ProductVersion + Environment.NewLine;
            lineSingleLog += "" + Environment.NewLine; ;
            lineSingleLog += "RESULT:                " + myLowerStructure.testRESULT + Environment.NewLine; ;
            lineSingleLog += "" + Environment.NewLine; ;
            lineSingleLog += "TEST list" + Environment.NewLine; ;
            //lineSingleLog += "Connections:           " + myLowerStructure.chekConnessioni + Environment.NewLine;
            //lineSingleLog += "Input BN:              " + myLowerStructure.checkIngresso + Environment.NewLine;
            //lineSingleLog += "Cassettes install:     " + myLowerStructure.asmCassettes + Environment.NewLine; ;
            //lineSingleLog += "Alarm board:           " + myLowerStructure.connAllarmi + Environment.NewLine;
            //lineSingleLog += "Interlock:             " + myLowerStructure.connInterlock + Environment.NewLine;
            lineSingleLog += "Power ON:              " + myLowerStructure.powerOn + Environment.NewLine;
            lineSingleLog += "Machine state:         " + myLowerStructure.chekStato + Environment.NewLine;
            lineSingleLog += "FW install:            " + myLowerStructure.fwInstalled + Environment.NewLine; ;
            lineSingleLog += " Suite code:           " + myLowerStructure.suiteCode + Environment.NewLine; ;
            lineSingleLog += " FW file name:         " + myLowerStructure.fwFileName + Environment.NewLine;
            lineSingleLog += " Safe Ver Installed:   " + myLowerStructure.safeVerInstalled + Environment.NewLine;
            lineSingleLog += " Safe Rel Installed:   " + myLowerStructure.safeRelInstalled + Environment.NewLine;
            lineSingleLog += "MIV-IN motor test:     " + myLowerStructure.testMivIn + Environment.NewLine;
            lineSingleLog += "MIV-OUT motor test:    " + myLowerStructure.testMivOut + Environment.NewLine;
            lineSingleLog += "Photo cash R+L adjust: " + myLowerStructure.adjFotoCash + Environment.NewLine;
            lineSingleLog += "        FCASH L value: " + myLowerStructure.fCashLValue + Environment.NewLine;
            lineSingleLog += "        FCASH R value: " + myLowerStructure.fCashRValue + Environment.NewLine;
            lineSingleLog += "Tray open test:        " + myLowerStructure.testVassoio + Environment.NewLine;
            lineSingleLog += "Elm test:              " + myLowerStructure.testElm + Environment.NewLine;

            string localLogFileName = localLogForlder + "\\" + myLowerStructure.moduleSerialNumber + ".txt";
            string serverLogFileName = iniConfig.GetValue("link", "logfolder") + myLowerStructure.moduleSerialNumber + ".txt";

            using (StreamWriter myLocalLog = File.AppendText(localLogFileName))
                myLocalLog.WriteLine(lineSingleLog);
            using (StreamWriter myServerLog = File.AppendText(serverLogFileName))
                myServerLog.WriteLine(lineSingleLog);
        }

        string TestElm(int testId = 7)
        {
            start:
            string result = FAIL;
            string reply = "";
            messaggio(19);//Connetti il kit Elm e premi un tasto per attivarlo
            lblTest[testId].BackColor = Color.Red;
            picImage.Visible = true;
            picImage.Image = Properties.Resources.KitElm;
            AttendiTasto();
            connessione:
            int cmConReply = myDll.CMConnect(ref cmConnectionParam);
            if (cmConReply != 0)
            {
                if (cmConReply == 1052)
                {
                    myDll.CMDisconnect();
                    goto connessione;
                }
                messaggio(30, cmConReply); //Non è stato possibile connettersi alla seriale, risposta {0}. Premere un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
            }
            reply = myDll.CMSingleCommand("B,1,6,1", 2);
            if (reply != "101" && reply != "1")
            {
                messaggio(53, reply); //Test ELM fallito. Risposta {0} Premi un tasto per ripetere, ESC per uscire
                if (AttendiTasto() == 27)
                    goto fine;
                goto start;
            }
            //attivazione elettromagnete
            messaggio(18);//L'elettromagnete ha funzionato? Premi un tasto per continuare, ESC per uscire
            if (AttendiTasto() == 27)
                goto fine;
            lblTest[testId].BackColor = Color.Lime;
            result = PASS;
            fine:
            myLowerStructure.testElm = result;
            myDll.CMSingleCommand("B,1,6,0");
            myDll.CMDisconnect();
            picImage.Image = null;
            picImage.Visible = false;
            return result;
        }
        private void cboPartNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboPartNumber.SelectedIndex < 0)
                return;
            cboClient.Items.Clear();
            cboClient.Text = "";
            FillDbInControl("SELECT DISTINCT clienti.nome_cliente FROM tblman,clienti WHERE tblman.id_prodotto = '" + cboPartNumber.Text + "' AND clienti.id_cliente = tblman.id_cliente", cboClient);
            messaggio(3); //Seleziona il cliente
            cboClient.Enabled = true;
            cboClient.Focus();
            myLowerStructure.productCode = cboPartNumber.Text;
            cboPartNumber.Enabled = false;

        }


        private void cboClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboPartNumber.Enabled = false;
            cboClient.Enabled = false;
            sqlCommand = "SELECT * FROM prodotti WHERE id_prodotto = '" + cboPartNumber.Text + "'";
            using (myConnection = new OleDbConnection(myConnectionString))
            {
                dbCommand = new OleDbCommand(sqlCommand, myConnection);
                try
                {
                    myConnection.Open();
                    dbReader = dbCommand.ExecuteReader();
                    while (dbReader.Read())
                    {
                        lblProductInfo.Text = cboPartNumber.Text + " - " + dbReader["descrizione"].ToString();
                        myLowerStructure.productFamily = dbReader["categoria_prodotto"].ToString();
                    }
                    dbReader.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }

            sqlCommand = "SELECT * FROM clienti WHERE nome_cliente = '" + cboClient.Text + "'";
            using (myConnection = new OleDbConnection(myConnectionString))
            {
                dbCommand = new OleDbCommand(sqlCommand, myConnection);
                try
                {
                    myConnection.Open();
                    dbReader = dbCommand.ExecuteReader();
                    while (dbReader.Read())
                        myLowerStructure.clientId = Convert.ToInt16(dbReader["id_cliente"].ToString());
                    dbReader.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }

            sqlCommand = "SELECT * FROM tblman WHERE id_prodotto = '" + cboPartNumber.Text + "' AND id_cliente = " + myLowerStructure.clientId + " AND id_modulo = 'FW_PRODOTTO'";
            using (myConnection = new OleDbConnection(myConnectionString))
            {
                dbCommand = new OleDbCommand(sqlCommand, myConnection);
                try
                {
                    myConnection.Open();
                    dbReader = dbCommand.ExecuteReader();
                    while (dbReader.Read())
                        myLowerStructure.suiteCode= dbReader["cod_fw"].ToString();
                    dbReader.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                lblProductInfo.Text += " - " + myLowerStructure.suiteCode;
            }
            messaggio(4); //Premi un tasto per confermare, ESC per ripetere l'inserimento di part number e cliente
            if (Arca.WaitingKey() == "ESC")
            {
                cboClient.Items.Clear();
                cboPartNumber.Text = "";
                cboPartNumber.Enabled = true;
                cboPartNumber.Focus();
                lblProductInfo.Text = "";
                messaggio(2); //Seleziona il part number
                
            }
            else
            {
                myLowerStructure.productCode = cboPartNumber.Text;
                myLowerStructure.clientName = cboClient.Text;
                elmTest = iniConfig.GetValue("pnelmtest", myLowerStructure.productCode);
                messaggio(9); //Inserisci il serial number
                cboClient.Visible = false;
                cboPartNumber.Visible = false;
                txtCode.Visible = true;
                txtCode.Focus();
            }

        }

        
        private new void KeyUp(object sender, KeyEventArgs e)
        {
            Arca.keyCodePressed = e.KeyValue;
        }



        private void txtCode_KeyUp(object sender, KeyEventArgs e)
        {
            if (Arca.keyCodePressed == 13)
            {
                //validazione codice inserito??
                if (txtCode.Text.Length != 12)
                {
                    messaggio(54); //Il serial number inserito non è corretto. Deve avere 12 caratteri
                    txtCode.Text = "";
                    txtCode.Focus();
                }
                else
                {
                    txtCode.Visible = false;
                    btnStart.Visible = true;
                    myLowerStructure.moduleSerialNumber = txtCode.Text;
                    myLowerStructure.moduleCode = txtCode.Text.Substring(0, 5);
                    messaggio(55); //Premi START per avviare il collaudo
                }
                //this.AcceptButton = btnStart;
            }
        }
    }
}
