	unicode true
; File da includere
	!include "MUI2.nsh"

; Dati applicazione
	!define APPNAME "CMLowerStructureTest"
	Name "${APPNAME}"
	!define COMPANYNAME "ARCA Technologies"
	!define VERSIONMAJOR 1
	!define VERSIONMINOR 0
	!define VERSIONBUILD 1

; Impostazione cartelle di sorgente, output ed installazione
	!define INSTFOLDER "C:\ARCA\${APPNAME}\"
	!define SOURCEFOLDER "C:\VisualStudioProject\CMLowerStructureTest\CMLowerStructureTest\bin\Debug\"
	OutFile "${APPNAME} V${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONBUILD}.exe"
	
; Impostazione Icona installazione/disinstallazione
	!define MUI_ICON "C:\VisualStudioProject\NSISimages\icona.ico"
	!define MUI_UNICON "C:\VisualStudioProject\NSISimages\icona.ico"
	
	!define MUI_ABORTWARNING
	
; Impostazione header (bmp 150x57 24bit) GIMP:EsportaComeBmp->'NonScrivereInfoColore'+24bitR8G8B8
	!define MUI_HEADERIMAGE_BITMAP "C:\VisualStudioProject\NSISimages\headerlogo.bmp"
	!define MUI_HEADERIMAGE
	
; Impostazione pagina di benvenuto (testo ed immagine bmp 164x314 24bit) GIMP:EsportaComeBmp->'NonScrivereInfoColore'+24bitR8G8B8
	!define txtMessageLine1 "Software guidato per il collaudo del modulo inferiore della famiglia CMxx"
	!define txtMessageLine2 ""
	!define txtMessageLine3 ""
	!define MUI_WELCOMEPAGE_TITLE "Installazione CM Lower Structure Test"
	!define MUI_WELCOMEPAGE_TEXT "${txtMessageLine1}$\r$\n$\r$\n${txtMessageLine2}$\r$\n$\r$\n${txtMessageLine3}"
	!define MUI_WELCOMEFINISHPAGE_BITMAP "C:\VisualStudioProject\NSISimages\welcomelogo.bmp"
	!insertmacro MUI_PAGE_WELCOME
	
; Impostazione pagina della licenza
	!insertmacro MUI_PAGE_LICENSE "C:\VisualStudioProject\NSISimages\ArcaLicense.txt"
	
; Personalizzazione cartella d'installazione
	;!insertmacro MUI_PAGE_DIRECTORY
	
	
	!insertmacro MUI_PAGE_COMPONENTS
	
; Visualizzazione avanzamento installazione file
	!insertmacro MUI_PAGE_INSTFILES

	; Impostazione conferma di disinstallazione
	!insertmacro MUI_UNPAGE_CONFIRM
	
; Visualizzazione avanzamento disinstallazione file
	!insertmacro MUI_UNPAGE_INSTFILES
	
; Visualizzazione pagin installazione terminata
	;!define MUI_FINISHPAGE_TITLE "Titolo pagina finale"
	;!define MUI_FINISHPAGE_TEXT "Testo pagina finale"
	!define MUI_FINISHPAGE_NOAUTOCLOSE
    !define MUI_FINISHPAGE_RUN
    !define MUI_FINISHPAGE_RUN_NOTCHECKED
	!define MUI_FINISHPAGE_RUN_TEXT "Avvia CMLowertStructureTest"
	!define MUI_FINISHPAGE_RUN_FUNCTION "LaunchApplication"
	!insertmacro MUI_PAGE_FINISH

; Impostazione linguaggio d'installazione
	!insertmacro MUI_LANGUAGE "English"
	
Function LaunchApplication
	MessageBox MB_OK "Reached LaunchLink $\r$\n \
                   SMPROGRAMS: $SMPROGRAMS  $\r$\n \
                   Start Menu Folder: $STARTMENU_FOLDER $\r$\n \
                   InstallDirectory: $INSTDIR "
	ExecShell "" "${INSTFOLDER}\XXX.exe"
FunctionEnd
	

	
Section "CMLowerStructureTest"
	SectionIn RO
	SetOutPath "${INSTFOLDER}"
	File "${SOURCEFOLDER}\CMCommand.dll"
	File "${SOURCEFOLDER}\CMLink.dll"
	File "${SOURCEFOLDER}\CMTrace.dll"
	File "${SOURCEFOLDER}\CMLowerStructureTest.exe"
	File "${SOURCEFOLDER}\Dino.dll"
	File "${SOURCEFOLDER}\italiano.ini"
	File "${SOURCEFOLDER}\setup.ini"
	File "${SOURCEFOLDER}\Aggiorna CMLowerStructureTest.bat"
	
	CreateShortCut "$DESKTOP\${APPNAME}.lnk" "${INSTFOLDER}\.exe" ""
	WriteUninstaller "${INSTFOLDER}\Uninstall.exe"
SectionEnd



Section "Uninstall"
	RMDir /r "${INSTFOLDER}\*.*"
	RMDir "${INSTFOLDER}"
	Delete "$DESKTOP\${APPNAME}.lnk"
SectionEnd